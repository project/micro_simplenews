<?php

namespace Drupal\micro_simplenews\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\micro_site\Entity\Site;
use Drupal\micro_site\Entity\SiteInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class SimplenewsForm.
 */
class MicroSimplenewsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new FetchProvidersEntitiesGroupService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'micro_simplenews_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['micro_simplenews.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SiteInterface $site = NULL) {
    if (!$site instanceof SiteInterface) {
      $form = [
        '#type' => 'markup',
        '#markup' => $this->t('Simplenews settings is only available in a micro site context.'),
      ];
      return $form;
    }

    $form['site_id'] = [
      '#type' => 'value',
      '#value' => $site->id(),
    ];

    $form['simplenews_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Check this option to enabled the Simplenews. Uncheck to disable the Simplenews.'),
      '#default_value' => (bool) !empty($site->getData('micro_simplenews')),
    ];

    $simplenews_newsletters = $this->entityTypeManager->getStorage('simplenews_newsletter')->loadMultiple();
    $options = [];

    foreach ($simplenews_newsletters as $key => $object) {
      $options[$key] = $object->label();
    }

    $form['simplenews_newsletters'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#title' => $this->t('Simplenews Newsletters'),
      '#default_value' => $site->getData('micro_simplenews')['simplenews_newsletters'] ?? NULL,
      '#states' => [
        'invisible' => [
          'input[name="simplenews_enabled"]' => ['checked' => FALSE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $site_id = $form_state->getValue('site_id');
    $site = Site::load($site_id);
    if (!$site instanceof SiteInterface) {
      $form_state->setError($form, $this->t('An error occurs. Impossible to find the site entity.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $site_id = $form_state->getValue('site_id');
    $site = Site::load($site_id);
    if (!$site instanceof SiteInterface) {
      return;
    }
    $data = [];

    if (empty($form_state->getValue('simplenews_enabled'))) {
      $site->setData('micro_simplenews', $data);
      $site->save();
    }
    else {
      $values = $form_state->getValues();
      // Save config.
      foreach ($values as $key => $value) {
        if (isset($value) && !in_array($key, [
          'form_id',
          'form_build_id',
          'form_token',
          'op',
          'submit',
        ])) {
          $data[$key] = $value;
        }
      }
      $site->setData('micro_simplenews', $data);
      $site->save();
    }
  }

}
