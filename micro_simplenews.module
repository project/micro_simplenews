<?php

/**
 * @file
 * Contains micro_simplenews.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_help().
 */
function micro_simplenews_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the micro_simplenews module.
    case 'help.page.micro_simplenews':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provide specific implementations for Simplenews per micro site') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function micro_simplenews_form_node_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Add Simplenews settings to simplenews newsletter node form.
  $node = $form_state->getFormObject()->getEntity();
  if (in_array($node->getType(), simplenews_get_content_types())) {
    /** @var \Drupal\micro_site\SiteNegotiatorInterface $negotiator */
    $negotiator = \Drupal::service('micro_site.negotiator');
    /** @var \Drupal\micro_site\Entity\SiteInterface $site */
    if ($site = $negotiator->getActiveSite()) {
      $config = $site->getData('micro_simplenews');
      if ($config['simplenews_enabled']) {
        $options = $form['simplenews_issue']['widget']['target_id']['#options'];
        $simplenews_newsletters = array_filter($config['simplenews_newsletters'], function ($v, $k) {
          return $v !== 0;
        }, ARRAY_FILTER_USE_BOTH);
        $form['simplenews_issue']['widget']['target_id']['#options'] = array_intersect_key($options, $simplenews_newsletters);
      }
    }
  }
}
